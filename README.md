# Proiect TW

Aplicație web pentru partajarea experiențelor utilizării mijloacelor de transport

	Structura echipei

Project manager ~ Streza Alexandru

Product manager ~ Stoian Maria

Developer ~ Suciu Cristina, Șerban Sorina, Storociuc Ana Maria  
 
	Obiectiv
	
Realizarea unei aplicații web prin care utilizatorii pot împărtășii experiența din urma utilizării unuia din mijloacele de transport în comun.


	Descriere

Aplicația trebuie sa permita crearea unui cont prin care utilizatorul poate sa partajeze o experiența, după ce a folosit un mijloc de transport în comun. 

Pentru utilizatorii anonimi, aplicația va permite căutarea și vizualizarea intrărilor în platformă.

