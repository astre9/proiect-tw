import "./components/theme/assets/scss/black-dashboard-react.scss";
import "./components/theme/assets/css/nucleo-icons.css";
import "./components/css/login.css";
import '../node_modules/react-toastify/dist/ReactToastify.css';import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'
import App from './components/App';
import Register from './components/Register';
import Login from './components/Login';
import AddExperience from './components/AddExperience';
import Experiences from './components/Experiences';
import MyExperiences from './components/MyExperiences';
import UserProfile from './components/UserProfile';

const routing = (
  <Router>
    <div>
      <Route path="/" component={App} />
      <Route path="/register" component={Register} />
      <Route path="/profile" component={UserProfile} />
      <Route path="/login" component={Login} />
      <Route path="/addexperience" component={AddExperience} />
      <Route path="/add" component={AddExperience} />
      <Route path="/experiences" component={Experiences} />
      <Route path="/myexperiences" component={MyExperiences} />
    </div>
  </Router>
)

ReactDOM.render(routing, document.getElementById('root'))
