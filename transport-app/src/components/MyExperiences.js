import React, { Component } from "react";
import Sidebar from "./theme/components/Sidebar/Sidebar.jsx";
import TablePagination from "./Pagination";
import { ToastContainer, toast } from 'react-toastify';
import "./theme/assets/scss/black-dashboard-react.scss";
import "./theme/assets/css/nucleo-icons.css";
import "./css/experiences.css";

import axios from 'axios';

import {
    Button,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Input,
    Row,
    Table,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
}
from "reactstrap";

class MyExperiences extends Component {
    constructor(props) {
        super(props);
        this.state = {
            start_point: '',
            end_point: '',
            transport: 'Transport',
            departure_hour: null,
            crowding_degree: 0,
            comments: '',
            duration: 0,
            satisfaction_level: '',
            date_added: null,
            experiences: []
        };
    }

    handleChangeStartPoint = (event) => {
        this.setState({ start_point: event.target.value });
    };

    handleChangeEndPoint = (event) => {
        this.setState({ end_point: event.target.value });
    };

    handleChangeTransport = (event) => {
        this.setState({ transport: event.target.value });

        console.log(event.target.value);
    };
    handleEdit = e => {
        e.preventDefault();
        let id = e.target.id;
        let experience = this.state.experiences.filter((value) => {
            return value.id == id;
        });
        console.log(experience);
        this.props.history.push({
            pathname: `/add`,
            search: `?id=${e.target.id}`,
            state: {
                exp: experience[0]
            }
        })
    }
    handleAdd = e => {
        e.preventDefault();
        this.props.history.push('/add')
    }

    handleDelete = e => {
        e.preventDefault();
        let id = e.target.id;
        axios
            .delete(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/experiences/delete/${id}`, {})
            .then((result) => {
                console.log(result.data)
                let experiences = this.state.experiences.filter((value) => {
                    return value.id != id;
                });
                this.setState({
                    experiences: experiences
                })
                toast.info("Experience deleted!");
            })
            .catch(err => {
                console.error(err);
            });
    }

    handleFilter = e => {
        e.preventDefault();
        var { start_point, end_point, transport } = this.state;
        var self = this;
        let id = sessionStorage.getItem("id")
        if (transport == "Transport") {
            transport = "";
        }
        axios.get(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/experiences/user/${id}?sp=${start_point}&ep=${end_point}&t=${transport}`)
            .then((result) => {
                console.log(result.data)
                self.getData(result.data)
            })
            .catch(err => {
                console.error(err);
            })
    };

    FormatTime(time, prefix = "") {
        var date = new Date(time);
        // console.log(date) // returns NaN if it can't parse
        return Number.isNaN(date) ? "" : prefix + date.toLocaleDateString();
    }

    getData(data) {
        var experiences = [];

        if (data.length == 0) {

        }
        else {
            data.forEach((exp) => {
                let experience = {};
                experience.id = exp.id;
                experience.start_point = exp.start_point;
                experience.end_point = exp.end_point;
                experience.transport = exp.transport;
                experience.comments = exp.comments;
                experience.satisfaction_level = exp.satisfaction_level;
                experience.duration = exp.duration;
                // let dep_hour = new Date(exp.departure_hour); // Valid Date
                // experience.departure_hour = dep_hour.getUTCHours() + ":" + dep_hour.getUTCMinutes();
                experience.departure_hour = exp.departure_hour;
                experience.crowding_degree = exp.crowding_degree;
                experience.date_added = exp.date_added;
                experiences.push(experience);
            })
        }
        console.log(experiences)
        this.setState({
            experiences: experiences
        })
    }
    renderTableData() {
        return this.state.experiences.map((experience, index) => {
            const { id, start_point, end_point, transport, comments, duration, satisfaction_level, departure_hour, crowding_degree, date_added } = experience;

            return (
                <tr className="text-center" key={id}>
                  <td>{id}</td>
                  <td>{start_point}</td>
                  <td>{end_point}</td>
                  <td>{transport}</td>
                  <td>{departure_hour}</td> 
                  <td>{duration}</td> 
                  <td>{crowding_degree}</td>
                  <td>{satisfaction_level}</td>
                  <td>{comments}</td>
                  <td>{this.FormatTime(date_added)}</td>
                  <td>
                      <Button className="mr-2 btn-round btn-icon" color="warning" id={id}  onClick={this.handleEdit}>
                            <i className="tim-icons icon-pencil" id={id} start_point={start_point}/>
                        </Button>
                           <Button className="mr-2 btn-round btn-icon" id={id} color="danger"  onClick={this.handleDelete}>
                            <i className="tim-icons icon-trash-simple" id={id} />
                        </Button>
                  </td>
                </tr>
            )
        })
    }
    componentDidMount() {
        var self = this;
        var id = sessionStorage.getItem("id");

        axios.get(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/experiences/user/${id}`, {})
            .then(function(response) {
                self.getData(response.data)
            })
            .catch(function(error) {
                console.log(error);
            });
    }
    render() {
        return (
            <div className="content centered-card">
                <Card className="card-experiences">
                    <CardHeader>
                        <h5 className="title">Experiences</h5>
                    </CardHeader>
                    <CardBody>
                        <Row className="ml-3">
                            <Col className="col-md-2">
                                <Input value={this.state.start_point} onChange={this.handleChangeStartPoint} placeholder="Location" type="text" />
                            </Col>
                            <Col className="col-md-2">
                                <Input value={this.state.end_point} onChange={this.handleChangeEndPoint} placeholder="Destination" type="text" />
                            </Col>
                            <Col md="1">
                                <UncontrolledDropdown > 
                                    <DropdownToggle caret color="primary" data-toggle="dropdown" style = {{marginTop:"5px"}}>
                                        {this.state.transport}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        <DropdownItem onClick={this.handleChangeTransport} value="transport">All</DropdownItem>
                                        <DropdownItem divider/>
                                        <DropdownItem onClick={this.handleChangeTransport} value="bus">Bus</DropdownItem>
                                        <DropdownItem divider/>
                                        <DropdownItem onClick={this.handleChangeTransport} value="tram">Tram</DropdownItem>
                                        <DropdownItem divider/>
                                        <DropdownItem onClick={this.handleChangeTransport} value="subway">Subway</DropdownItem>
                                        <DropdownItem divider/>
                                        <DropdownItem onClick={this.handleChangeTransport} value="maxitaxi">Maxitaxi</DropdownItem>
                                        <DropdownItem divider/>
                                        <DropdownItem onClick={this.handleChangeTransport} value="trolleybus">Trolleybus</DropdownItem>
                                    </DropdownMenu>
                                </UncontrolledDropdown>
                            </Col>
                            <Col>
                                <Button className="mr-3" color="info" onClick={this.handleFilter}>
                                        <i className="tim-icons icon-zoom-split" />{" "} Filter
                                </Button>
                                <Button className="mr-2 btn-round btn-icon" color="success" onClick={this.handleAdd}>
                                        <i className="tim-icons icon-simple-add" />
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col md="12">
                                <Table responsive style={{height:"150%"}}>
                                    <thead>
                                        <tr className="text-center">
                                            <th>ID</th>
                                            <th>Start point</th>
                                            <th>End point</th>
                                            <th>Transport</th>
                                            <th>Departure hour</th>
                                            <th>Minutes</th>
                                            <th>Crowding degree</th>
                                            <th>Satisfaction Level</th>
                                            <th>Comments</th>
                                            <th>Date added</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        { this.renderTableData() } 
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </CardBody>
                    <CardFooter>
                    <TablePagination/>
                    </CardFooter>
                </Card>
        </div>
        );
    }
}

export default MyExperiences;
