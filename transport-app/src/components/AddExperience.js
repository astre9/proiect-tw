import React, { Component } from "react";
import { ToastContainer, toast } from 'react-toastify';
import "./theme/assets/scss/black-dashboard-react.scss";
import "./theme/assets/css/nucleo-icons.css";
import "./css/add.css";
import "./css/react-datetime.css";
import 'url-search-params-polyfill';
import Datetime from 'react-datetime';
import axios from 'axios';

import {
    Button,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    FormGroup,
    Input,
    Row,
    Col,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem

}
from "reactstrap";

class AddExperience extends Component {

    constructor(props) {
        super(props);

        this.state = {
            start_point: '',
            end_point: '',
            transport: 'Transport',
            departure_hour: '',
            crowding_degree: 'Crowding',
            comments: '',
            duration: 0,
            satisfaction_level: '😐',
            date_added: new Date(),
            button: 'Add',
            edit: false
        }
    }
    handleStartPoint = (event) => {
        this.setState({ start_point: event.target.value });
    };
    handleEndPoint = (event) => {
        this.setState({ end_point: event.target.value });
    };
    handletransport = (event) => {
        this.setState({ transport: event.target.value });
    }
    handleDuration = (event) => {
        this.setState({ duration: event.target.value });
    }
    handleDepartureHour = (date) => {
        this.setState({ departure_hour: date });
    }
    handleCrowdingDegree = (event) => {
        const parsedDegree = parseInt(event.target.value)
        this.setState({ crowding_degree: parsedDegree });
    }
    handleComments = (event) => {
        this.setState({ comments: event.target.value });
    }
    handleSatisfactionLevel = (event) => {
        this.setState({ satisfaction_level: event.target.value });
    }
    handleDateAdded = (event) => {
        this.setState({ date_added: event.target.value });
    }

    handleSubmit = e => {
        e.preventDefault();
        let userId = sessionStorage.getItem("id");
        const { start_point, end_point, transport, departure_hour, crowding_degree, comments, duration, satisfaction_level, date_added } = this.state;
        const exp = {
            start_point,
            end_point,
            transport,
            departure_hour,
            duration,
            crowding_degree,
            comments,
            satisfaction_level,
            date_added,
            user: userId
        };
        var self = this;
        if (exp.start_point && exp.end_point && exp.crowding_degree && exp.duration > 0 && exp.departure_hour && exp.satisfaction_level && exp.transport) {
            if (this.state.edit == false) {
                axios
                    .post(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/experiences/add`, exp)
                    .then((result) => {
                        console.log(result.data.message);
                        toast.success("Experience added!");
                        self.setState(() => ({
                            visible: true,
                            toLogin: true
                        }))
                        self.props.history.push("/myexperiences")
                    })
                    .catch(err => {
                        toast.error("Check all required fields!");
                        console.error(err);
                    });
            }
            else {
                axios
                    .put(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/experiences/${this.state.id}/update`, exp)
                    .then(function(response) {
                        toast.info("Experience updated!");
                        self.props.history.push("/myexperiences")
                        console.log(response)
                    })
                    .catch(function(error) {
                        toast.error("Check all required fields!");
                        console.log(error);
                    });

            };
        }
        else {

        }
    }



    onDismiss = () => {
        this.setState({ visible: false });
    };

    componentDidMount() {
        if (this.props.location.state != null) {
            let exp = this.props.location.state.exp;
            this.setState({
                start_point: exp.start_point,
                end_point: exp.end_point,
                transport: exp.transport,
                duration: exp.duration,
                departure_hour: exp.departure_hour,
                crowding_degree: exp.crowding_degree,
                comments: exp.comments,
                satisfaction_level: exp.satisfaction_level,
                date_added: exp.date_added,
                edit: true,
                button: "Save",
                id: exp.id
            })
        }
        else {

        }
    };

    render() {
        return (
            <div>
                <div className="content centered-card">
                        <form onSubmit={this.handleSubmit}>
                            <Card className="card-add">
                                <CardHeader>
                                    <h5 className="title">Add/Edit Experiences</h5>
                                </CardHeader>
                                <CardBody>
                                    <Row className="mr-1">
                                        <Col className="pr-md-1" md="3">
                                            <FormGroup>
                                                <label>Start Point</label>
                                                <Input value={this.state.start_point} onChange={this.handleStartPoint} placeholder="StartPoint" type="text" />
                                            </FormGroup>
                                        </Col>
            
                                        <Col className="pr-md-1" md="4">
                                            <FormGroup>
                                                <label>End Point</label>
                                                <Input value={this.state.end_point} onChange={this.handleEndPoint} placeholder="EndPoint" type="text" />
                                            </FormGroup>
                                        </Col>
            
                                        <Col className="pr-md-1" md="5">
                                            <FormGroup>
                                               <label>Transport</label>
                                               <UncontrolledDropdown > 
                                                <DropdownToggle caret color="primary" data-toggle="dropdown" >
                                                    {this.state.transport}
                                                </DropdownToggle>
                                                    <DropdownMenu>
                                                        <DropdownItem onClick={this.handletransport} value="bus">Bus</DropdownItem>
                                                        <DropdownItem divider/>
                                                        <DropdownItem onClick={this.handletransport} value="tram">Tram</DropdownItem>
                                                        <DropdownItem divider/>
                                                        <DropdownItem onClick={this.handletransport} value="subway">Subway</DropdownItem>
                                                        <DropdownItem divider/>
                                                        <DropdownItem onClick={this.handletransport} value="maxitaxi">Maxitaxi</DropdownItem>
                                                        <DropdownItem divider/>
                                                        <DropdownItem onClick={this.handletransport} value="trolleybus">Trolleybus</DropdownItem>
                                                    </DropdownMenu>
                                                </UncontrolledDropdown>
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className="pr-md-1" md="6">
                                            <FormGroup>
                                                <label>Departure Hour</label>
                                                <Datetime dateFormat={false} value={this.state.departure_hour} onChange={this.handleDepartureHour} placeholder="Departure Hour" />
                                            </FormGroup>
                                        </Col>

                                        <Col className="pr-md-1" md="6">
                                            <FormGroup>
                                                <label>Duration</label>
                                                <Input value={this.state.duration} onChange={this.handleDuration} placeholder="Minutes" type="number" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                        <Row className="mt-2 mb-2">
                                            <Col className="pr-md-1" md="4">
                                                <FormGroup>
                                                    <label>Crowding</label>
                                                    <UncontrolledDropdown defaultValue={this.state.crowding_degree} >
                                                        <DropdownToggle caret caret color="primary" data-toggle="dropdown">
                                                            {this.state.crowding_degree}
                                                        </DropdownToggle>
                                                        <DropdownMenu>
                                                            <DropdownItem onClick={this.handleCrowdingDegree} value="1">1</DropdownItem>
                                                            <DropdownItem onClick={this.handleCrowdingDegree} value="2">2</DropdownItem>
                                                            <DropdownItem onClick={this.handleCrowdingDegree} value="3">3</DropdownItem>
                                                            <DropdownItem onClick={this.handleCrowdingDegree} value="4">4</DropdownItem>
                                                            <DropdownItem onClick={this.handleCrowdingDegree} value="5">5</DropdownItem>
                
                                                        </DropdownMenu>
                                                    </UncontrolledDropdown>
                                                </FormGroup>
                                            </Col>        
                                            <Col className="pr-md-1" md="6">
                                                <FormGroup>
                                                    <label>Satisfaction</label>
                                                    <UncontrolledDropdown defaultValue={this.state.satisfaction_level}>
                                                        <DropdownToggle caret caret color="primary" data-toggle="dropdown">
                                                            {this.state.satisfaction_level}
                                                        </DropdownToggle>
                                                        <DropdownMenu>
                                                            <DropdownItem onClick={this.handleSatisfactionLevel} value="😢">😢</DropdownItem>
                                                            <DropdownItem onClick={this.handleSatisfactionLevel} value="😐">😐</DropdownItem>
                                                            <DropdownItem onClick={this.handleSatisfactionLevel} value="🙂">🙂</DropdownItem>
                                                            <DropdownItem onClick={this.handleSatisfactionLevel} value="😃">😃</DropdownItem>
                                                            <DropdownItem onClick={this.handleSatisfactionLevel} value="😍">😍</DropdownItem>
                                                        </DropdownMenu>
                                                    </UncontrolledDropdown>
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                    <Row>
                                        <Col md="12">
                                            <FormGroup>
                                                <label>Comments</label>
                                                <Input cols="80" value={this.state.comments} onChange={this.handleComments} placeholder="Here you can add a short comment about your experience" rows="8" type="textarea" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </CardBody>
                                <CardFooter>
                                    <div className="text-center mb-3">
                                        <Button className="btn-fill" id="button"  color="primary" type="submit">
                                     {this.state.button}
                                        </Button>
                                    </div>
                                </CardFooter>
                            </Card>
                        </form>
                </div>
            </div>

        )
    }

}

export default AddExperience
