import React, { Component } from "react";
import { ToastContainer, toast } from 'react-toastify';
import { Redirect } from 'react-router-dom'
import { Alert } from "reactstrap";

import "./theme/assets/scss/black-dashboard-react.scss";
import "./theme/assets/css/nucleo-icons.css";
import "./css/register.css";

import axios from 'axios';

import {
    Button,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    FormGroup,
    Input,
    Row,
    Col
}
from "reactstrap";

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            email: '',
            first_name: '',
            last_name: '',
            about: '',
            toLogin: false,
            visible: false
        };
    }

    handleChangeUsername = (event) => {
        this.setState({ username: event.target.value });
    };

    handleChangePassword = (event) => {
        this.setState({ password: event.target.value });
    };

    handleChangeEmail = (event) => {
        this.setState({ email: event.target.value });
    };

    handleChangeFName = (event) => {
        this.setState({ first_name: event.target.value });
    };

    handleChangeLName = (event) => {
        this.setState({ last_name: event.target.value });
    };

    handleChangeAbout = (event) => {
        this.setState({ about: event.target.value });
    };

    handleSubmit = e => {
        e.preventDefault();
        const { username, password, email, last_name, first_name, about } = this.state;
        const newUser = Object.assign({}, this.state);
        console.log(newUser);
        const usr = {
            username,
            password,
            email,
            last_name,
            first_name,
            about
        };
        console.log(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/register`);
        axios
            .post(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/register`, usr)
            .then((result) => {
                console.log(result.data.message);
                toast.success("Account created successfully!");
                this.setState(() => ({
                     visible: true,
                    toLogin: true
                }))
            })
            .catch(err => {
                toast.error("Check all required fields!");
                console.error(err);
            });

    };
    onDismiss = () => {
        this.setState({ visible: false });
    };
    
    render() {
        if (this.state.toLogin === true) {
            return <Redirect to='/login' />
        }

        return (
        <div>
            <Alert color="info" className="text-center" isOpen={this.state.visible}
                  toggle={this.onDismiss}>
                <strong>Great!</strong> Account created sucessfully!
            </Alert>
            <div className="content centered-card">
                <Row>
                    <Col md="12">
                        <form onSubmit={this.handleSubmit}>
                            <Card className="card-register">
                                <CardHeader>
                                    <h5 className="title">Register</h5>
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col className="pr-md-1" md="3">
                                            <FormGroup>
                                                <label>Username</label>
                                                <Input value={this.state.username} onChange={this.handleChangeUsername} placeholder="Username" type="text" />
                                            </FormGroup>
                                        </Col>
                                        <Col className="pr-md-1" md="4">
                                            <FormGroup>
                                                <label>Password</label>
                                                <Input value={this.state.password} onChange={this.handleChangePassword} placeholder="Password" type="password" />
                                            </FormGroup>
                                        </Col>
                                        <Col className="pl-md-1" md="5">
                                            <FormGroup>
                                                <label htmlFor="exampleInputEmail1">
                                                    Email address
                                                </label>
                                                <Input value={this.state.email} onChange={this.handleChangeEmail} placeholder="E-mail" type="email" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col className="pr-md-1" md="6">
                                            <FormGroup>
                                                <label>First Name</label>
                                                <Input value={this.state.first_name} onChange={this.handleChangeFName} placeholder="First Name" type="text" />
                                            </FormGroup>
                                        </Col>
                                        <Col className="pl-md-1" md="6">
                                            <FormGroup>
                                                <label>Last Name</label>
                                                <Input value={this.state.last_name} onChange={this.handleChangeLName} placeholder="Last Name" type="text" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md="12">
                                            <FormGroup>
                                                <label>About Me</label>
                                                <Input cols="80" value={this.state.about} onChange={this.handleChangeAbout} placeholder="Here can be your description" rows="8" type="textarea" />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                </CardBody>
                                <CardFooter>
                                    <div className="text-center mb-3">
                                        <Button className="btn-fill" color="primary" type="submit">
                                            Register
                                        </Button>
                                    </div>
                                </CardFooter>
                            </Card>
                        </form>
                    </Col>
                </Row>
            </div>
        </div>
        );
    }
}

export default Register;
