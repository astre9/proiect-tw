import React, { Component } from "react";
import { Redirect } from 'react-router-dom'
import { Alert } from "reactstrap";
import { ToastContainer, toast } from 'react-toastify';

import "./theme/assets/scss/black-dashboard-react.scss";
import "./theme/assets/css/nucleo-icons.css";
import "./css/userprofile.css";

import axios from 'axios';

import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  FormGroup,
  Form,
  Input,
  Row,
  Col
}
from "reactstrap";

class UserProfile extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      first_name: '',
      last_name: '',
      about: '',
      id: sessionStorage.getItem("id")
    }
  }

  handleChangeEmail = (event) => {
    this.setState({ email: event.target.value });
  };

  handleChangePassword = (event) => {
    this.setState({ password: event.target.value });
  };

  handleChangeFName = (event) => {
    this.setState({ first_name: event.target.value });
  };

  handleChangeLName = (event) => {
    this.setState({ last_name: event.target.value });
  };

  handleChangeAbout = (event) => {
    this.setState({ about: event.target.value });
  };

  handleExperiences = e => {
    e.preventDefault();
    this.props.history.push("/myexperiences")
  }

  handleDelete = e => {
    e.preventDefault();
    axios
      .delete(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/profile/delete/${this.state.id}`, {})
      .then((result) => {
        console.log(result.data)
        sessionStorage.removeItem("id")
        this.props.history.push("/login")
        toast.success("Account deactivated!");
      })
      .catch(err => {
        console.error(err);
      });
  }

  handleSubmit = e => {
    e.preventDefault();
    const { email, last_name, first_name, about, id, password } = this.state;
    const usr = {
      email,
      last_name,
      first_name,
      about,
      id,
      password
    };
    axios
      .put(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/profile/update`, usr)
      .then((result) => {
        console.log(result.data.message)
        toast.info("Account updated!");
      })
      .catch(err => {
        console.error(err);
        toast.error("Check all required fields!");
      });
  };
  componentDidMount() {
    var self = this;
    var id = sessionStorage.getItem("id");
    console.log(id);
    axios.get(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/profile/${id}`, {})
      .then(function(response) {
        self.setState({
          email: response.data.email,
          password: response.data.password,
          last_name: response.data.last_name,
          first_name: response.data.first_name,
          about: response.data.about
        })
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    return (
      <div className="content centered-card">
          <Row>
            <Col md="10">
              <Card className="card-userprofile">
                <CardHeader>
                  <h5 className="title">Edit Profile
                    <Button className="btn-simple ml-3" onClick={this.handleExperiences} size="sm" color="info">My experiences</Button>
                    <Button className="btn-simple" onClick={this.handleDelete} style={{position:"absolute", right:"25px"}} size="sm" color="danger">Delete</Button>
                  </h5>
                </CardHeader>
                <CardBody>
                  <Form onSubmit={this.handleSubmit}>
                    <Row>
                      <Col className="pl-md-1 ml-2" md="5">
                        <FormGroup>
                          <label htmlFor="exampleInputEmail1">
                            Email address
                          </label>
                          <Input value={this.state.email} 
                          onChange={this.handleChangeEmail} 
                          placeholder="E-mail" 
                          type="email" />
                        </FormGroup>
                      </Col>
                    <Col className="pl-md-1" md="4">
                        <FormGroup>
                          <label htmlFor="pass">
                            Password
                          </label>
                          <Input value={this.state.password} 
                          onChange={this.handleChangePassword} 
                          placeholder="Password" 
                          type="text" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-md-1" md="5">
                        <FormGroup>
                          <label>First Name</label>
                          <Input
                            value={this.state.first_name} 
                            onChange={this.handleChangeFName} 
                            placeholder="First Name" 
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pl-md-1" md="6">
                        <FormGroup>
                          <label>Last Name</label>
                          <Input
                            value={this.state.last_name} 
                            onChange={this.handleChangeLName} 
                            placeholder="Last Name" 
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>About Me</label>
                          <Input
                            cols="80"
                            value={this.state.about} 
                            onChange={this.handleChangeAbout} 
                            placeholder="Here can be your description" 
                            rows="8" 
                            type="textarea"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
                <CardFooter>
                    <div className="text-center mb-3">
                        <Button className="btn-fill" color="primary" onClick={this.handleSubmit}>
                            Save
                        </Button>
                    </div>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </div>
    );
  }
}
export default UserProfile;
