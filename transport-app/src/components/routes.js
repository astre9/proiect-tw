import Login from "./Login.js";
import Register from "./Register.js";
import UserProfile from "./UserProfile.js";
import Experiences from "./Experiences.js";
import MyExperiences from "./MyExperiences.js";
import AddExperience from "./AddExperience.js";

var loggedInRoutes = [{
    path: "/profile",
    name: "Profile",
    rtlName: "الرموز",
    icon: "tim-icons icon-single-02",
    component: UserProfile,
    layout: ""
  },
  {
    path: "/experiences",
    name: "Experiences",
    rtlName: "الرموز",
    icon: "tim-icons icon-bullet-list-67",
    component: Experiences,
    layout: ""
  },
  {
    path: "/myexperiences",
    name: "My experiences",
    rtlName: "الرموز",
    icon: "tim-icons icon-bullet-list-67",
    component: MyExperiences,
    layout: ""
  },
  {
    path: "/add",
    name: "Add experience",
    rtlName: "الرموز",
    icon: "tim-icons icon-simple-add",
    component: AddExperience,
    layout: ""
  },
  {
    path: "/login",
    name: "Log out",
    rtlName: "الرموز",
    icon: "tim-icons icon-double-left",
    component: Login,
    layout: ""
  }
];


var loggedOutRoutes = [{
    path: "/register",
    name: "Register",
    rtlName: "الرموز",
    icon: "tim-icons icon-single-02",
    component: Register,
    layout: ""
  },
  {
    path: "/login",
    name: "Login",
    rtlName: "الرموز",
    icon: "tim-icons icon-double-left",
    component: Login,
    layout: ""
  }
];

export default { loggedInRoutes, loggedOutRoutes }
