import React, { Component } from "react";
import { ToastContainer, toast } from 'react-toastify';
import axios from 'axios';
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    FormGroup,
    Input,
    Row,
    Col
}
from "reactstrap";

require('dotenv').config()


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: ''
        };
    }

    handleChangeUsername = (event) => {
        this.setState({ username: event.target.value });
    };

    handleChangePassword = (event) => {
        this.setState({ password: event.target.value });
    };

    handleSubmit = e => {
        e.preventDefault();
        const { username, password } = this.state;
        const user = Object.assign({}, this.state);
        console.log(user);
        const usr = {
            username,
            password,
        };
        axios
            .post(`http://${process.env.REACT_APP_IP}:${process.env.REACT_APP_PORT}/login`, usr)
            .then((result) => {
                sessionStorage.setItem("id", result.data.id);
                this.props.history.push('/experiences');
                toast.success("Login succesfull!");

            })
            .catch(err => {
                toast.error("Login failed!");
            })
    };
    componentDidMount() {
        sessionStorage.removeItem("id");
    }
    render() {
        return (
            <div className="content centered-card">
          <Row>
            <Col md="12">
                 <form onSubmit={this.handleSubmit}>
              <Card className="login-card">
               
                <CardHeader>
                  <h5 className="title">Login</h5>
                </CardHeader>
                <CardBody>
                    <Row>
                      <Col className="pr-md-3 pl-md-3">
                        <FormGroup>
                          <label>Username</label>
                          <Input
                            value={this.state.username}
                            onChange={this.handleChangeUsername}
                            placeholder="Username"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      </Row>
                      <Row>
                       <Col className="pr-md-3 pl-md-3">
                        <FormGroup>
                          <label>Password</label>
                          <Input
                            value={this.state.password}
                            onChange={this.handleChangePassword}
                            placeholder="Password"
                            type="password"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                </CardBody>
                    <CardFooter>
                        <div className="text-center">
                            <Button className="btn-fill" color="primary" type="submit">
                                Sign in
                            </Button>
                        </div>
                    </CardFooter>
                </Card>
              </form>
            </Col>
          </Row>
        </div>
        );
    }
}

export default Login;
