import { toast } from 'react-toastify';
import React, { Component } from 'react';
import Sidebar from "./theme/components/Sidebar/Sidebar.jsx";
import loggedInRoutes from "./routes"
import loggedOutRoutes from "./routes"

class App extends Component {
  constructor(props) {
    super(props);
    toast.configure();
    
    this.state = {
      backgroundColor: "blue",
      sidebarOpened: document.documentElement.className.indexOf("nav-open") !== -1,
      routes: loggedInRoutes.loggedInRoutes
    };

  }
  componentDidMount() {
    setInterval(() => {
      if (sessionStorage.getItem("id")) {
        this.setState({
          routes: loggedInRoutes.loggedInRoutes
        });
      }
      else {
        this.setState({
          routes: loggedOutRoutes.loggedOutRoutes
        });
      }
    }, 100)
  }
  render() {
    return (
      <div>
        <Sidebar
                // this is necessary so we can check for the active link
            {...this.props}
            routes={Object.values(this.state.routes)}
            bgColor={this.state.backgroundColor}
            // this is necessary so we can close the menu, when a users goes to another page
            toggleSidebar={this.toggleSidebar}
            />
      </div>
    )
  }
}

export default App;
