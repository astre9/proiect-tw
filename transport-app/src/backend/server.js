const express = require('express')
const bodyParser = require('body-parser')
const { sequelize, Experience, User } = require('./models');
const cors = require('cors');
const Sequelize = require("sequelize");

const app = express();
app.use(bodyParser.json());
app.use(
    cors({
        origin: `http://18.219.208.88:8080`,
        credentials: true,
    })
);

app.locals.userId = 0;


app.get("/", (request, response) => {
    response.status(200).send("This is the homepage!");
});

app.post('/register', (request, response) => {
    const user = request.body;
    if (user.username && user.email && user.password) {
        User.findOne({ where: { email: user.email } }).then(result => {
            if (result) {
                response.status(409).send({ message: "E-mail already in use!" })
            }
            else {
                User.build(user).save().then(user => {
                    response.status(201).send({ message: "Account created successfully!" });
                })
            }
        })
    }
    else {
        response.status(400).send({ message: 'Missing mandatory fields.' });
    }
})

app.put('/profile/update', (request, response) => {
    const user = request.body;
    if (user.email && user.password) {
        User.findOne({ where: { email: user.email } }).then(result => {
            if (result) {
                User.update(user, { where: { id: user.id } }).then(user => {
                    response.status(201).send({
                        message: "User created successfully"
                    });
                })
            }
            else {
                response.status(404).send({
                    message: "User doesn't exist"
                });
            }
        })
    }
    else {
        response.status(400).send({
            message: 'Missing user fields'
        });
    }
})

app.post('/login', (request, response) => {
    const credentials = request.body;
    console.log("POST /login");
    User.findOne({ where: { username: credentials.username, password: credentials.password } }).then(result => {
        if (result) {
            response.status(200).send({ message: "Login successful!", id: result.id });
        }
        else {
            response.status(404).send({ message: 'Invalid credentials!' })
        }
    })
})

app.get('/profile/:userId', (request, response) => {
    const userId = request.params.userId;
    const experienceId = request.params.experienceId;
    User.findOne({ where: { id: userId } }).then(result => {
        response.status(200).send(result);
    });
})

app.delete('/profile/delete/:userId', (request, response) => {
    const userId = request.params.userId;
    User.destroy({
        where: {
            id: userId
        }
    }).then(() => {
        response.status(204).send({ message: "User account deactivated!" })
    })

})

// app.get('/experiences', (request, response) => {
//     const userId = app.locals.userId;
//     Experience.findAll({ where: { userId: userId } }).then(result => {
//         response.status(200).send(result);
//     });
// })

app.get('/experiences/:experienceId', (request, response) => {
    const userId = app.locals.userId;
    const experienceId = request.params.experienceId;
    Experience.findOne({ where: { userId: userId, id: experienceId } }).then(result => {
        response.status(200).send(result);
    });
})

app.post('/experiences/add', (request, response) => {
    const experience = request.body;
    const userId = experience.user;
    if (experience.start_point && experience.end_point && experience.transport && experience.crowding_degree && experience.satisfaction_level) {
        User.findOne({ where: { id: userId } }).then(result => {
            if (result) {
                experience.userId = userId;
                Experience.create(experience).then(() => {
                    response.status(201).send({
                        message: 'Experience successfully added to user.'
                    })
                })
            }
            else {
                response.status(404).send({ message: "User not found." })
            }
        });
    }
    else {
        response.status(500).send({ message: 'Missing required fields.' })
    }
})

app.put('/experiences/:experienceId/update', (request, response) => {
    const experienceId = request.params.experienceId;
    const experience = request.body;
    console.log(experienceId)
    if (experience.start_point && experience.end_point && experience.transport && experience.crowding_degree) {
        Experience.update({
            start_point: experience.start_point,
            end_point: experience.end_point,
            duration: experience.duration,
            transport: experience.transport,
            crowding_degree: experience.crowding_degree,
            departure_hour: experience.departure_hour,
            comments: experience.comments
        }, { where: { id: experienceId } }).then(result => {
            if (result[0]) {
                console.log(result);
                response.status(200).send({
                    message: `Updated: ${result[0]} experiences!`
                })
            }
            else {
                response.status(404).send({
                    message: "No matching records."
                })
            }
        }).catch(err => {
            response.status(500).send(err);
        })
    }
    else {
        response.status(500).send({ message: 'Missing required fields.' })
    }
})

app.delete('/experiences/delete/:experienceId', (request, response) => {
    const experienceId = request.params.experienceId;
    Experience.destroy({ where: { id: experienceId } }).then(result => {
        if (result) {
            response.status(200).send({
                message: `Deleted ${result} experiences!`
            });
        }
        else {
            response.status(200).send({
                message: `No records found matching the id!`
            })
        }
    }).catch(err => {
        response.status(500).send(err);
    })
})

app.get('/experiences/user/:userId', (request, response) => {
    const exper = {
        start_point: request.query.sp,
        end_point: request.query.ep,
        transport: request.query.t
    };
    const id = request.params.userId;
    console.log(exper)

    if (exper.start_point || exper.transport || exper.end_point) {
        Experience.findAll({
            where: {
                [Sequelize.Op.and]: [{
                    [Sequelize.Op.or]: [{
                            start_point: {
                                [Sequelize.Op.like]: exper.start_point
                            }
                        },
                        {
                            end_point: {
                                [Sequelize.Op.like]: exper.end_point
                            }
                        },
                        {
                            transport: exper.transport
                        }
                    ]
                }, { userId: id }],
            }
        }).then(result => {
            response.status(200).send(result);
        });
    }
    else {
        Experience.findAll({ where: { userId: id } }).then(result => {
            response.status(200).send(result);
        });
    }
})

app.get('/experiences', (request, response) => {
    const exper = {
        start_point: request.query.sp,
        end_point: request.query.ep,
        transport: request.query.t
    };
    const id = request.params.userId;
    console.log(exper)

    if (exper.start_point || exper.transport || exper.end_point) {
        Experience.findAll({
            include: [{
                model: User
            }],
            where: {
                [Sequelize.Op.or]: [{
                        start_point: {
                            [Sequelize.Op.like]: exper.start_point
                        }
                    },
                    {
                        end_point: {
                            [Sequelize.Op.like]: exper.end_point
                        }
                    },
                    {
                        transport: exper.transport
                    }
                ]
            }
        }).then(result => {
            console.log(result)
            response.status(200).send(result);
        });
    }
    else {
        Experience.findAll({
            include: [{
                model: User
            }]
        }).then(result => {
            response.status(200).send(result);
        });
    }
})

app.listen(3001, () => {
    console.log('Server started on port 3001...');
})
