const Sequelize = require('sequelize');
const mysql = require('mysql2/promise')

const DB_USERNAME = 'root'
const DB_PASSWORD = 'P@ss1234'

mysql.createConnection({
        user: DB_USERNAME,
        password: DB_PASSWORD,
        charset: 'utf8mb4' // pentru emoji
    })
    .then(async(connection) => {
        await connection.query("SHOW DATABASES")
        await connection.query("CREATE DATABASE IF NOT EXISTS transport_db")
        await connection.query("ALTER DATABASE transport_db CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;")// pentru emoji
        await connection.query("USE transport_db;")
        await connection.query("ALTER TABLE experiences CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;") // pentru emoji
    })
    .catch((err) => {
        console.warn(err.stack)
    })

const sequelize = new Sequelize('transport_db', 'root', DB_PASSWORD, {
    dialect: 'mysql',
    define: {
        timestamps: false
    }
});

// sequelize.authenticate().then(() => {
//     console.log('Connection established successfully!');
// }).catch(err => {
//     console.log(`Error on connection: ${err}`);
// })


class User extends Sequelize.Model {}
User.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    password: Sequelize.STRING,
    first_name: Sequelize.STRING,
    last_name: Sequelize.STRING,
    about: Sequelize.STRING
}, { sequelize, modelName: 'users' });


class Experience extends Sequelize.Model {}
Experience.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    start_point: Sequelize.STRING,
    end_point: Sequelize.STRING,
    transport: Sequelize.STRING,
    departure_hour: Sequelize.DATE,
    crowding_degree: Sequelize.INTEGER,
    duration: Sequelize.INTEGER,
    comments: Sequelize.STRING,
    satisfaction_level: Sequelize.STRING,
    date_added: Sequelize.DATEONLY

}, { sequelize, modelName: 'experiences' })

User.hasMany(Experience);
Experience.belongsTo(User, {foreignKey: 'userId'})

sequelize.sync();

module.exports = {
    sequelize,
    User,
    Experience
}
